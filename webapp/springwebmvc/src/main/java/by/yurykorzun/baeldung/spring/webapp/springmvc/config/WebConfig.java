package by.yurykorzun.baeldung.spring.webapp.springmvc.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan("by.yurykorzun.baeldung.spring.webapp.springmvc")
public class WebConfig {
}
