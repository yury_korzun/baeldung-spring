package by.yurykorzun.baeldung.spring.security.service;

import by.yurykorzun.baeldung.spring.security.entities.Foo;

import java.util.Optional;

public interface IFooService {

    Optional<Foo> findById(Long id);

    Foo save(Foo foo);

    Iterable<Foo> findAll();
}
