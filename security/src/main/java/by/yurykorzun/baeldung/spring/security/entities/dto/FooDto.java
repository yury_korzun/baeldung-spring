package by.yurykorzun.baeldung.spring.security.entities.dto;

public class FooDto {
    private long id;
    private String name;

    public FooDto(long id, String name) {
        this.id = id;
        this.name = name;
    }
}
