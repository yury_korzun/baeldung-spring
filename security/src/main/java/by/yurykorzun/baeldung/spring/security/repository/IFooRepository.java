package by.yurykorzun.baeldung.spring.security.repository;

import by.yurykorzun.baeldung.spring.security.entities.Foo;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IFooRepository extends PagingAndSortingRepository<Foo, Long> {
}