package by.yurykorzun.baeldung.spring.security.service.impl;

import by.yurykorzun.baeldung.spring.security.entities.Foo;
import by.yurykorzun.baeldung.spring.security.repository.IFooRepository;
import by.yurykorzun.baeldung.spring.security.service.IFooService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FooServiceImpl implements IFooService {

    IFooRepository repository;

    @Autowired
    public FooServiceImpl(IFooRepository repository){
        this.repository = repository;
    }

    @Override
    public Optional<Foo> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Foo save(Foo foo) {
        return repository.save(foo);
    }

    @Override
    public Iterable<Foo> findAll() {
        return repository.findAll();
    }
}
