package by.yurykorzun.baeldung.spring.security.controller;

import by.yurykorzun.baeldung.spring.security.entities.Foo;
import by.yurykorzun.baeldung.spring.security.entities.dto.FooDto;
import by.yurykorzun.baeldung.spring.security.service.IFooService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/foos")
public class FooController {

    private IFooService service;

    @Autowired
    public FooController(IFooService service){
        this.service = service;
    }

    @CrossOrigin(origins = "http://localhost:8089")
    @GetMapping("/{id}")
    public FooDto findOne(@PathVariable Long id){
        Foo entity = service.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return convertToDto(entity);


    }

    @GetMapping
    public Collection<FooDto> findAll(){
        Iterable<Foo> foos = service.findAll();
        List<FooDto> fooDtos = new ArrayList<>();
        foos.forEach(f -> fooDtos.add(convertToDto(f)));
        return fooDtos;
    }

    protected FooDto convertToDto(Foo entity){
        FooDto dto = new FooDto(entity.getId(), entity.getName());
        return dto;
    }

}
